﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KongMing.Project.BackTask
{
    public class DemoBgService : BackgroundService
    {
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.CancelAfter(20000);
            do
            {
                if (cts.IsCancellationRequested)
                    break;
                string s = await File.ReadAllTextAsync("e:/TxtFiles/1.txt");
                Console.WriteLine(s);
                await Task.Delay(3000);
            } while (true);
        }
    }
}