﻿using KongMing.Project.Entity.Business;
using KongMing.Project.Entity.Common;

namespace KongMing.Project.IService.Common
{
    public interface IAuthService
    {
        public Task<BaseApiResult> LoginByAccountAndPwdAsync(string Account,string pwd);
    }
}