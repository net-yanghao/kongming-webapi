﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KongMing.Project.Entity.Business;
using KongMing.Project.Entity.Common;
using KongMing.Project.Entity.Dto;
using KongMing.Project.Entity.View;

namespace KongMing.Project.IService.Business
{
    public interface IUserService
    {
        public Task<BaseApiResult> UserSave(SysUser user);

        public Task<User_V> UserDetail(string userId);

        public Task<QueryResult<User_V>> UserList(QueryModel<User_R> query);
    }
}
