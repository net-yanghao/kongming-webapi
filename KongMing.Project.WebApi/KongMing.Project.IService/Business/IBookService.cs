﻿using KongMing.Project.Entity.Business;
using KongMing.Project.Entity.Common;
using KongMing.Project.Entity.Dto;
using KongMing.Project.Entity.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.IService.Business
{
    public interface IBookService
    {
        public Task<Book_V> BookDetailAsync(string bookId);

        public Task BookSaveAsync(Book_R book);

        public Task<QueryResult<Book_V>> BookQueryAsync(QueryModel<Book_R> query);
    }
}
