﻿// See https://aka.ms/new-console-template for more information
using KongMing.Project.Entity;
using static System.Console;
using SqlSugar;
using KongMing.Project.Entity.Business;


WriteLine("Hello, World!");

var db = new SqlSugarClient(new ConnectionConfig()
{
    ConnectionString = "server=.;Database=KongMing.BusinessDB;Uid=km;Pwd=666",
    DbType = DbType.SqlServer,
    IsAutoCloseConnection = true,
    InitKeyType =InitKeyType.Attribute
});

WriteLine("确定同步实体类到数据库？");
var res = ReadKey();
if (res.Key == ConsoleKey.Enter)
    WriteLine("同步数据库表结构...");
else
    return;

//同步数据库表结构
db.DbMaintenance.CreateDatabase();
db.CodeFirst.SetStringDefaultLength(50).BackupTable().InitTables(new Type[]
{
    typeof(SysUser),
    typeof(Book),
    typeof(SysAuthToken),
    typeof(SysMenu)
});
WriteLine("数据库结构同步完成!");