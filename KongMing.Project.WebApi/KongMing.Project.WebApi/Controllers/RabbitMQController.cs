﻿using Microsoft.AspNetCore.Mvc;
using KongMing.Project.Common.Sdk;
using KongMing.Project.Common;
using System.Text;

namespace KongMing.Project.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class RabbitMQController : BaseApiController
    {
        //IRabbitMQService _rabbitMQ;
        RabbitMQClient _rabbitMQ;
        public RabbitMQController(IServiceProvider serviceProvider):base(serviceProvider)
        {
            GetService<RabbitMQClient>();
        }

        [HttpPost]
        public void PushMessage(object data)
        {
            string msg = LibConvert.ObjToStr(data).Trim();
            RabbitMQClient.NormalClient.Value.Push(Consts.Exchange_Direct_OrderPush, Consts.RouteKey_OrderPush, msg);
        }

        [HttpGet]
        public string GetMessage()
        {
            RabbitMQClient.NormalClient.Value.Connect();
            var result = RabbitMQClient.NormalClient.Value.channel.BasicGet(Consts.Quene_OrderPush, true);
            string? msg = result is null ? null : Encoding.UTF8.GetString(result.Body.ToArray());
            return msg;
        }
    }
}
