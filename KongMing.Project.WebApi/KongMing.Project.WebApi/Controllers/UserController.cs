﻿using KongMing.Project.Common;
using KongMing.Project.Entity.Dto;
using KongMing.Project.Entity.View;
using KongMing.Project.Entity.Business;
using KongMing.Project.Entity.Common;
using KongMing.Project.IService.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KongMing.Project.WebApi.Controllers
{
    [Route("[controller]/[action]")]   
    [ApiController]
    public class UserController : BaseApiController
    {
        private IUserService _userService;
        public UserController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _userService = GetService<IUserService>();
        }
        [HttpPost]
        public Task<BaseApiResult> UserSave(SysUser user) => _userService.UserSave(user);

        [HttpGet]
        public Task<User_V> UserDetail(string userId) => _userService.UserDetail(userId);

        [HttpPost]
        public Task<QueryResult<User_V>> UserList(QueryModel<User_R> query) => _userService.UserList(query);
    }
}
