﻿using KongMing.Project.Entity.Common;
using KongMing.Project.IService.Common;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace KongMing.Project.WebApi.Controllers
{
    [EnableCors("cors")]
    [Route("[controller]/[action]")]
    public class AuthController : ControllerBase
    {
        private IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpGet]
        public async Task<BaseApiResult> LoginAsync(string account, string pwd)
        {
            return await _authService.LoginByAccountAndPwdAsync(account, pwd);
        }
    }
}
