﻿using KongMing.Project.Common;
using KongMing.Project.Entity.Common;
using KongMing.Project.Entity.Dto;
using KongMing.Project.Entity.View;
using KongMing.Project.IService.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KongMing.Project.WebApi.Controllers
{
    /// <summary>
    /// 图书管理
    /// </summary>
    public class BookController : BaseApiController
    {
        private IBookService service;
        public BookController(IServiceProvider serviceProvider) : base(serviceProvider) 
        {
            service = GetService<IBookService>();
        }

        [HttpGet]
        public async Task<Book_V> BookDetail(string bookId)
        {
            return await service.BookDetailAsync(bookId);
        }

        [HttpPost]
        public async Task BookSave(Book_R book)
        {
            await service.BookSaveAsync(book);
        }
        
        [HttpPost]
        public async Task<QueryResult<Book_V>> BookQuery(QueryModel<Book_R> query)
        {
            return await service.BookQueryAsync(query);
        }
    }
}
