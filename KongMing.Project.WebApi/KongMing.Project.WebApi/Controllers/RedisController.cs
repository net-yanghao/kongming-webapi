﻿using KongMing.Project.Common.Sdk;
using Microsoft.AspNetCore.Mvc;
using KongMing.Project.Common;

namespace KongMing.Project.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class RedisController : BaseApiController
    {
        private RedisHelper redisHelper;
        public RedisController(IServiceProvider serviceProvider):base(serviceProvider) 
        {
            redisHelper = new RedisHelper(GetService<RedisConnectionHelp>());
        }

        [HttpGet]
        public string StringGet(string key) 
        {
            var res = redisHelper.StringGet(key);
            return res;
        }

        [HttpGet]
        public bool StringSet(string key, string value) => redisHelper.StringSet(key, value);

        #region Hash哈希表
        [HttpGet]
        public bool KeyExists(string key) => redisHelper.KeyExists(key);

        [HttpGet]
        public object GetHash(string key) => redisHelper.HashKeysAsync(key).Result;

        [HttpGet]
        public object HashGet(string key, string hashkey) => redisHelper.HashGet<object>(key, hashkey);

        [HttpGet]
        public bool HashSet(string key, string hashkey, string value) => redisHelper.HashSet(key, hashkey, value);

        [HttpGet]
        public bool HashExit(string key, string hashkey) => redisHelper.HashExists(key, hashkey);
        #endregion

        #region List列表
        /// <summary>
        /// 获取集合
        /// </summary>
        [HttpGet]
        public List<string> GetList(string key) => redisHelper.ListRange(key);
        /// <summary>
        /// 向list左边添加元素
        /// </summary>
        [HttpGet]
        public void ListleftAdd(string key, string val) => redisHelper.ListLeftPush(key, val);
        /// <summary>
        /// 向list右边添加元素
        /// </summary>
        [HttpGet]
        public void ListrightAdd(string key, string val) => redisHelper.ListRightPush(key, val);
        /// <summary>
        /// 从list左边取出元素
        /// </summary>
        [HttpGet]
        public string ListleftRemove(string key) => redisHelper.ListLeftPopString(key);
        /// <summary>
        /// 从list右边取出元素
        /// </summary>
        [HttpGet]
        public string ListrightRemove(string key) => redisHelper.ListRightPop<string>(key);
        #endregion

        #region OrderSet
        [HttpGet]
        public bool SortedSetAdd(string key, string value, double score) => redisHelper.SortedSetAdd(key, value, score);
        #endregion
    }
}
