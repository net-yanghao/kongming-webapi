using Microsoft.OpenApi.Models;
using KongMing.Project.WebApi.Filters;
using KongMing.Project.IService.Business;
using KongMing.Project.Service.Business;
using KongMing.Project.Common.Sdk;
using KongMing.Project.Common.Filter;
using KongMing.Project.Common.ServiceExtension;
using KongMing.Project.Entity.Common;
using KongMing.Project.IService.Common;
using KongMing.Project.Service.Common;
using KongMing.Project.BackTask;

var builder = WebApplication.CreateBuilder(args);
var basePath = AppContext.BaseDirectory;

//引入配置文件
var _config = new ConfigurationBuilder()
                 .AddInMemoryCollection() //将配置文件的数据加载到内存中
                 .SetBasePath(basePath)//指定配置文件所在的目录
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                 //指定加载的配置文件  --**记得始终复制**
                 .Build();//编译        

//注册全部配置
builder.Services.AddConfigs(builder.Configuration);

#region 添加swagger注释
builder.Services.AddSwaggerGen(a =>
{
    a.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Api"
    });

    a.IncludeXmlComments(Path.Combine(basePath, "KongMing.Project.WebApi.xml"), true);
    a.DocumentFilter<SwaggerHideApiFilter>();//Swagger过滤掉不需要的api
    a.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "Value: Bearer {token}",//请求时，token前面需要加上Bearer，格式：Bearer token
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });
    a.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
      {
        new OpenApiSecurityScheme
        {
          Reference = new OpenApiReference
          {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer"
          },Scheme = "oauth2",Name = "Bearer",In = ParameterLocation.Header,
        },new List<string>()
      }
    });
});
#endregion

#region JWT鉴权
//注入Jwt配置
builder.Services.Configure<JwtConfig>(builder.Configuration.GetSection("JwtConfig"));
//Configuration.Bind，从appsettings.json中将指定配置读取到实体
JwtConfig jwtConfig = new JwtConfig();
builder.Configuration.Bind("JwtConfig",jwtConfig);
//自定义的IServicesCollection的拓展方法，注册jwt鉴权服务
builder.Services.AddJwtAuthentication(jwtConfig);
#endregion

#region 注入http上下文
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#endregion

builder.Services.AddHostedService<DemoBgService>();

//使用缓存
builder.Services.AddMemoryCache();

builder.Services.AddControllers(options =>
{
    options.Filters.Add(new GlobalExceptionFilter());//自定义全局异常处理过滤器
    options.Filters.Add(new GlobalApiResultFilter());//接口返回的数据格式处理过滤器
});
builder.Services.AddEndpointsApiExplorer();

#region 注册自定义服务
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IAuthService, AuthService>();
builder.Services.AddTransient<IBookService, BookService>();
//从appsettings.json中将指定配置读取到实体
//builder.Services.Configure<RabbitMQConfig>(builder.Configuration.GetSection("RabbitMQConfig"));
#endregion

#region 注册SqlSugar拓展
builder.Services.AddSqlSugarClients(_config);
#endregion

#region 注册RabbitMQ
RabbitMQConfig rabbitConfig = new RabbitMQConfig();
builder.Configuration.Bind("RabbitMQConfig", rabbitConfig);
builder.Services.AddSingleton(new RabbitMQClient(rabbitConfig));
#endregion

#region 注册跨域
builder.Services.AddCors(option =>
{
    option.AddPolicy("cors", a =>
    {
        a.AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
        .SetIsOriginAllowed(x =>
        {
            return true;
        });
    });
});
#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    
}
app.UseSwagger();
app.UseSwaggerUI();

app.UseCors("cors");
app.UseAuthentication();//使用jwt鉴权
app.UseAuthorization();//使用jwt授权

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.Run();
