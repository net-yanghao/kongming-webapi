﻿using KongMing.Project.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    /// <summary>
    /// 业务异常类
    /// </summary>
    public class BusinessException : Exception
    {
        /// <summary>
        /// 错误码
        /// </summary>
        public ErrorCode ErrorCode { get; set; }
        /// <summary>
        /// 错误数据
        /// </summary>
        public object? ErrorData { get; set; }

        public BusinessException(string msg) : base(msg)
        {
            ErrorCode = ErrorCode.BusinessError;
        }

        public BusinessException(ErrorCode errorCode, string msg) : base(msg)
        {
            ErrorCode = errorCode;
        }

        public BusinessException(ErrorCode errorCode, string msg, object data) : base(msg)
        {
            ErrorCode = errorCode;
            ErrorData = data;
        }
    }
}
