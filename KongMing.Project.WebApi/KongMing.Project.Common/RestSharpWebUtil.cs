﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    public class RestSharpWebUtil
    {
        private static int TimeOut = 20000;
        private static int ReadWriteTimeout = 20000;
        /// <summary>
        /// 发送Get请求
        /// </summary>
        public static string DoGet(string url,Dictionary<string,string> query,Dictionary<string,string> header) 
        {
            string result = string.Empty;
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.Timeout = TimeOut;
            request.ReadWriteTimeout = ReadWriteTimeout;
            foreach (var item in query)
                request.AddQueryParameter(item.Key, item.Value);
            foreach (var item in header)
                request.AddHeader(item.Key, item.Value);
            try
            {
                IRestResponse response = client.Execute(request);
                result = response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 发送Post请求
        /// </summary>
        public static string DoPost(string url, Dictionary<string, string> query, Dictionary<string, string> header, Dictionary<string, string> paramters, FileItem fileItem)
        {
            string result = string.Empty;
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.Timeout = TimeOut;
            request.ReadWriteTimeout = ReadWriteTimeout;
            foreach (var item in query)
                request.AddQueryParameter(item.Key, item.Value);
            foreach (var item in header)
                request.AddHeader(item.Key, item.Value);
            foreach (var item in paramters)
                request.AddParameter(item.Key, item.Value);
            if(fileItem != null)
            {
                if(!string.IsNullOrWhiteSpace(fileItem.Path) || fileItem.File?.Length > 0)
                {
                    if (fileItem.FileType == 0)
                        //上传二进制流文件
                        request.AddFile(fileItem.Key, fileItem.File, fileItem.FileName, fileItem.ContentType);
                    else
                        //上传本地路径下的文件
                        request.AddFile(fileItem.Key, fileItem.Path, fileItem.ContentType);
                }
            }
            try
            {
                IRestResponse response = client.Execute(request);
                result = response.Content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// 上传文件类
        /// </summary>
        public class FileItem
        {
            /// <summary>
            /// 文件类型：0二进制流文件，1：本地文件路径
            /// </summary>
            public int FileType { get; set; } = 0;
            /// <summary>
            /// 文件参数的key
            /// </summary>
            public string Key { get; set; }
            /// <summary>
            /// 文件名
            /// </summary>
            public string FileName { get; set; }
            /// <summary>
            /// 只用于本地文件,本地文件的路径
            /// </summary>
            public string Path { get; set; }
            /// <summary>
            /// 参数类型 application/x-www-form-urlencoded , multipart/form-data  , application/json  ,  application/octet-stream
            /// </summary>
            public string ContentType { get; set; }
            /// <summary>
            /// 文件二进制流
            /// </summary>
            public byte[] File { get; set; }
        }
    }
}
