﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    /// <summary>
    /// 从每次请求中的jwt token中解析出的信息类
    /// </summary>
    public class RequestUserContext
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserAccount { get; set; }

        public string JwtToken { get; set; }

        public HttpContext HttpContext { get; set; }

        public string CurrentRequestUrl { get; set; }

    }
}
