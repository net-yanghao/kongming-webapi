﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    public class Consts
    {
        #region RabbitMQ
        public const string Exchange_Topic_OrderPush = "ex_topic_orderpush";
        public const string Exchange_Direct_OrderPush = "ex_direct_orderpush";
        public const string Exchange_Fanout_OrderPush = "ex_fanout_orderpush";
        public const string Exchange_Headers_OrderPush = "ex_headers_orderpush";

        public const string Quene_OrderPush = "quene_orderpush";
        public const string RouteKey_OrderPush = "key_orderpush";
        #endregion
    }
}
