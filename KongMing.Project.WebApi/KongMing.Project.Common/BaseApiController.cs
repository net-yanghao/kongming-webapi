﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace KongMing.Project.Common
{
    [EnableCors("cors")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]//Jwt鉴权   
    [Route("[controller]/[action]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        protected IServiceProvider ServiceProvider { get; set; }
        public BaseApiController(IServiceProvider serviceProvider) 
        {
            ServiceProvider = serviceProvider;
        }

        protected string GetFullRequestUrl()
        {
            return $"{Request.Scheme}://{Request.Host.Value}{Request.Path}{Request.QueryString}";
        }

        protected T GetService<T>()=> ServiceProvider.GetService<T>();
    }
}
