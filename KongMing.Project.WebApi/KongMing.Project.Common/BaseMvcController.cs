﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace KongMing.Project.Common
{
    //[Authorize]//Jwt鉴权   
    [Route("[controller]/[action]")]
    public class BaseMVCController: Controller
    {
        protected IServiceProvider ServiceProvider { get; set; }
        public BaseMVCController(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        protected string GetFullRequestUrl()
        {
            return $"{Request.Scheme}://{Request.Host.Value}{Request.Path}{Request.QueryString}";
        }

        protected T GetService<T>() => ServiceProvider.GetService<T>();
    }
}
