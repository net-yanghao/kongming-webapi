﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    public static class Configs
    {
        public static DateTime Now { get { return DateTime.UtcNow.AddHours(8); } }
        public static string NowDate { get { return Now.ToString("yyyy-MM-dd"); } }
        public static string NowTime { get { return Now.ToString("yyyy-MM-dd HH:mm:ss"); } }
    }
}
