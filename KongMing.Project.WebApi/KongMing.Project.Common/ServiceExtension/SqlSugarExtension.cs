﻿using KongMing.Project.Common;
using KongMing.Project.Entity.Enum;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;

namespace KongMing.Project.Common.ServiceExtension
{
    /// <summary>
    /// SqlSugar上下文注入
    /// </summary>
    public static class SqlSugarExtension
    {
        public static void AddSqlSugarClients(this IServiceCollection services,IConfiguration _config) 
        {
            //主库
            services.AddScoped(o =>
            {
                var maindb = new MainDBContext(new ConnectionConfig()
                {
                    ConfigId = DBId.主库,
                    ConnectionString = _config.GetConnectionString("MainDBConnectString"),
                    DbType = DbType.SqlServer,
                    IsAutoCloseConnection = true
                });
                return maindb;
            });
            //业务库
            services.AddScoped(o =>
            {
                var bookdb = new BusinessDBContext(new ConnectionConfig()
                {
                    ConfigId = DBId.业务库,
                    ConnectionString = _config.GetConnectionString("BusinessDBConnectString"),
                    DbType = DbType.SqlServer,
                    IsAutoCloseConnection = true
                });
                return bookdb;
            });
        }
    }
}
