﻿using KongMing.Project.Entity.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common.ServiceExtension
{
    public static class ConfigExtension
    {
        public static void AddConfigs(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<RedisConfig>(config.GetSection("RedisConfig"));
        }
    }
}
