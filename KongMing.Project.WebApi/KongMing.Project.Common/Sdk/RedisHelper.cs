﻿using KongMing.Project.Common.Sdk;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Common.Sdk
{
    /// <summary>
    /// Redis操作类
    /// </summary>
    public class RedisHelper
    {
        private RedisConnectionHelp _redisHelp;
        public RedisHelper(RedisConnectionHelp redisHelp)
        {
            _redisHelp = redisHelp;
            DbNum = redisHelp._redisConfig.DefaultDB;
            _conn =
                string.IsNullOrWhiteSpace(redisHelp._redisConfig.ConnectionString) ?
                _redisHelp.Instance :
                _redisHelp.GetConnectionMultiplexer(redisHelp._redisConfig.ConnectionString);
        }

        #region 属性

        /// <summary>
        /// DB库
        /// </summary>
        private int DbNum { get; }

        /// <summary>
        /// Redis链接
        /// </summary>
        private readonly ConnectionMultiplexer _conn;

        #endregion

        #region String

        #region 同步方法

        /// <summary>
        /// 保存单个key value
        /// </summary>
        /// <param name="key">Redis Key</param>
        /// <param name="value">保存的值</param>
        /// <param name="expiry">过期时间</param>
        /// <returns></returns>
        public bool StringSet(string key, string value, TimeSpan? expiry = default(TimeSpan?)) =>
            Do(db => db.StringSet(key, value, expiry));

        /// <summary>
        /// 保存多个key value
        /// </summary>
        /// <param name="keyValues">键值对</param>
        /// <returns></returns>
        public bool StringSet(List<KeyValuePair<RedisKey, RedisValue>> keyValues)
        {
            List<KeyValuePair<RedisKey, RedisValue>> newkeyValues =
                keyValues.Select(p => new KeyValuePair<RedisKey, RedisValue>(p.Key, p.Value)).ToList();
            return Do(db => db.StringSet(newkeyValues.ToArray()));
        }

        /// <summary>
        /// 保存一个对象
        /// </summary>
        public bool StringSet<T>(string key, T obj, TimeSpan? expiry = default(TimeSpan?))
        {
            string json = ConvertJson(obj);
            return Do(db => db.StringSet(key, json, expiry));
        }

        /// <summary>
        /// 获取单个key的值
        /// </summary>
        public string StringGet(string key) => Do(db => db.StringGet(key));

        /// <summary>
        /// 获取多个Key
        /// </summary>
        public RedisValue[] StringGet(List<string> listKey)
        {
            List<string> newKeys = listKey.ToList();
            return Do(db => db.StringGet(ConvertRedisKeys(newKeys)));
        }

        /// <summary>
        /// 获取一个key的对象
        /// </summary>
        public T StringGet<T>(string key) => Do(db => ConvertObj<T>(db.StringGet(key)));

        /// <summary>
        /// 为数字增长val
        /// </summary>
        public double StringIncrement(string key, double val = 1) => Do(db => db.StringIncrement(key, val));

        /// <summary>
        /// 为数字减少val
        /// </summary>
        public double StringDecrement(string key, double val = 1) => Do(db => db.StringDecrement(key, val));

        #endregion 同步方法

        #region 异步方法

        /// <summary>
        /// 保存单个key value
        /// </summary>
        /// <param name="key">Redis Key</param>
        /// <param name="value">保存的值</param>
        /// <param name="expiry">过期时间</param>
        /// <returns></returns>
        public async Task<bool> StringSetAsync(string key, string value, TimeSpan? expiry = default(TimeSpan?)) => 
            await Do(db => db.StringSetAsync(key, value, expiry));

        /// <summary>
        /// 保存多个key value
        /// </summary>
        public async Task<bool> StringSetAsync(List<KeyValuePair<RedisKey, RedisValue>> keyValues)
        {
            List<KeyValuePair<RedisKey, RedisValue>> newkeyValues =
                keyValues.Select(p => new KeyValuePair<RedisKey, RedisValue>(p.Key, p.Value)).ToList();
            return await Do(db => db.StringSetAsync(newkeyValues.ToArray()));
        }

        /// <summary>
        /// 保存一个对象
        /// </summary>
        public async Task<bool> StringSetAsync<T>(string key, T obj, TimeSpan? expiry = default(TimeSpan?))
        {
            string json = ConvertJson(obj);
            return await Do(db => db.StringSetAsync(key, json, expiry));
        }

        /// <summary>
        /// 获取单个key的值
        /// </summary>
        public async Task<string> StringGetAsync(string key) => await Do(db => db.StringGetAsync(key));

        /// <summary>
        /// 获取多个Key
        /// </summary>
        public async Task<RedisValue[]> StringGetAsync(List<string> listKey)
        {
            List<string> newKeys = listKey.ToList();
            return await Do(db => db.StringGetAsync(ConvertRedisKeys(newKeys)));
        }

        /// <summary>
        /// 获取一个key的对象
        /// </summary>
        public async Task<T> StringGetAsync<T>(string key)
        {
            string result = await Do(db => db.StringGetAsync(key));
            return ConvertObj<T>(result);
        }

        /// <summary>
        /// 为数字增长val
        /// </summary>
        public async Task<double> StringIncrementAsync(string key, double val = 1) => 
            await Do(db => db.StringIncrementAsync(key, val));

        /// <summary>
        /// 为数字减少val
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val">可以为负</param>
        /// <returns>减少后的值</returns>
        public async Task<double> StringDecrementAsync(string key, double val = 1) => 
            await Do(db => db.StringDecrementAsync(key, val));

        #endregion 异步方法

        #endregion String

        #region Hash

        #region 同步方法

        /// <summary>
        /// 判断某个数据是否已经被缓存
        /// </summary>
        public bool HashExists(string key, string dataKey) => 
            Do(db => db.HashExists(key, dataKey));

        /// <summary>
        /// 存储数据到hash表
        /// </summary>
        public bool HashSet<T>(string key, string dataKey, T t)
        {
            return Do(db =>
            {
                string json = ConvertJson(t);
                return db.HashSet(key, dataKey, json);
            });
        }

        /// <summary>
        /// 移除hash中的某值
        /// </summary>
        public bool HashDelete(string key, string dataKey) => 
            Do(db => db.HashDelete(key, dataKey));

        /// <summary>
        /// 移除hash中的多个值
        /// </summary>
        public long HashDelete(string key, List<RedisValue> dataKeys) => 
            Do(db => db.HashDelete(key, dataKeys.ToArray()));

        /// <summary>
        /// 从hash表获取数据
        /// </summary>
        public T HashGet<T>(string key, string dataKey)
        {

            return Do(db =>
            {
                string value = db.HashGet(key, dataKey);
                return ConvertObj<T>(value);
            });
        }

        /// <summary>
        /// 从hash表获取数据
        /// </summary>
        public string HashGetString(string key, string dataKey)
        {
            return Do(db =>
            {
                return db.HashGet(key, dataKey);
            });
        }

        /// <summary>
        /// 为数字增长val
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dataKey"></param>
        /// <param name="val">可以为负</param>
        /// <returns>增长后的值</returns>
        public double HashIncrement(string key, string dataKey, double val = 1) => 
            Do(db => db.HashIncrement(key, dataKey, val));

        /// <summary>
        /// 为数字减少val
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dataKey"></param>
        /// <param name="val">可以为负</param>
        /// <returns>减少后的值</returns>
        public double HashDecrement(string key, string dataKey, double val = 1) =>
            Do(db => db.HashDecrement(key, dataKey, val));

        /// <summary>
        /// 获取hashkey所有Redis key
        /// </summary>
        public List<T> HashKeys<T>(string key)
        {
            return Do(db =>
            {
                RedisValue[] values = db.HashKeys(key);
                return ConvertList<T>(values);
            });
        }

        public List<string> HashKeysString<T>(string key) =>
            Do<List<string>>(db => this.ConvertString<string>(db.HashKeys(key, CommandFlags.None)));

        #endregion 同步方法

        #region 异步方法

        /// <summary>
        /// 判断某个数据是否已经被缓存
        /// </summary>
        public async Task<bool> HashExistsAsync(string key, string dataKey) =>
            await Do(db => db.HashExistsAsync(key, dataKey));

        /// <summary>
        /// 存储数据到hash表
        /// </summary>
        public async Task<bool> HashSetAsync<T>(string key, string dataKey, T t)
        {
            return await Do(db =>
            {
                string json = ConvertJson(t);
                return db.HashSetAsync(key, dataKey, json);
            });
        }

        /// <summary>
        /// 移除hash中的某值
        /// </summary>
        public async Task<bool> HashDeleteAsync(string key, string dataKey) =>
            await Do(db => db.HashDeleteAsync(key, dataKey));

        /// <summary>
        /// 移除hash中的多个值
        /// </summary>
        public async Task<long> HashDeleteAsync(string key, List<RedisValue> dataKeys) =>
            await Do(db => db.HashDeleteAsync(key, dataKeys.ToArray()));

        /// <summary>
        /// 从hash表获取数据
        /// </summary>
        public async Task<T> HashGeAsync<T>(string key, string dataKey)
        {
            string value = await Do(db => db.HashGetAsync(key, dataKey));
            return ConvertObj<T>(value);
        }

        /// <summary>
        /// 为数字增长val
        /// </summary>
        public async Task<double> HashIncrementAsync(string key, string dataKey, double val = 1) =>
            await Do(db => db.HashIncrementAsync(key, dataKey, val));

        /// <summary>
        /// 为数字减少val
        /// </summary>
        public async Task<double> HashDecrementAsync(string key, string dataKey, double val = 1) =>
            await Do(db => db.HashDecrementAsync(key, dataKey, val));

        /// <summary>
        /// 获取hashkey所有Redis key
        /// </summary>
        public async Task<List<string>> HashKeysAsync(string key)
        {
            RedisValue[] values = await Do(db => db.HashKeysAsync(key));
            return ConvertList(values);
        }

        #endregion 异步方法

        #endregion Hash

        #region List

        #region 同步方法

        /// <summary>
        /// 移除指定ListId的内部List的值
        /// </summary>
        public void ListRemove<T>(string key, T value) =>
            Do(db => db.ListRemove(key, ConvertJson(value)));

        /// <summary>
        /// 获取指定key的List
        /// </summary>
        public List<T> ListRange<T>(string key)
        {
            return Do(redis =>
            {
                var values = redis.ListRange(key);
                return ConvertList<T>(values);
            });
        }

        public List<string> ListRange(string key)
        {
            return Do(redis =>
            {
                var values = redis.ListRange(key);
                return ConvertList(values);
            });
        }

        /// <summary>
        /// 入队
        /// </summary>
        public void ListRightPush<T>(string key, T value) =>
            Do(db => db.ListRightPush(key, ConvertJson(value)));

        /// <summary>
        /// 出队
        /// </summary>
        public T ListRightPop<T>(string key)
        {
            return Do(db =>
            {
                var value = db.ListRightPop(key);
                return ConvertObj<T>(value);
            });
        }

        /// <summary>
        /// 入栈
        /// </summary>
        public void ListLeftPush<T>(string key, T value) =>
            Do(db => db.ListLeftPush(key, ConvertJson(value)));

        /// <summary>
        /// 出栈
        /// </summary>
        public T ListLeftPop<T>(string key)
        {
            return Do(db =>
            {
                var value = db.ListLeftPop(key);
                return ConvertObj<T>(value);
            });
        }

        /// <summary>
        /// 出栈
        /// </summary>
        public string ListLeftPopString(string key)
        {
            return Do(db =>
            {
                return db.ListLeftPop(key);
            });
        }

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        public long ListLength(string key) =>
            Do(redis => redis.ListLength(key));

        #endregion 同步方法

        #region 异步方法

        /// <summary>
        /// 移除指定ListId的内部List的值
        /// </summary>
        public async Task<long> ListRemoveAsync<T>(string key, T value) =>
            await Do(db => db.ListRemoveAsync(key, ConvertJson(value)));

        /// <summary>
        /// 获取指定key的List
        /// </summary>
        public async Task<List<T>> ListRangeAsync<T>(string key)
        {
            var values = await Do(redis => redis.ListRangeAsync(key));
            return ConvertList<T>(values);
        }

        /// <summary>
        /// 入队
        /// </summary>
        public async Task<long> ListRightPushAsync<T>(string key, T value) =>
            await Do(db => db.ListRightPushAsync(key, ConvertJson(value)));

        /// <summary>
        /// 出队
        /// </summary>
        public async Task<T> ListRightPopAsync<T>(string key)
        {
            var value = await Do(db => db.ListRightPopAsync(key));
            return ConvertObj<T>(value);
        }

        /// <summary>
        /// 入栈
        /// </summary>
        public async Task<long> ListLeftPushAsync<T>(string key, T value) =>
            await Do(db => db.ListLeftPushAsync(key, ConvertJson(value)));

        /// <summary>
        /// 出栈
        /// </summary>
        public async Task<T> ListLeftPopAsync<T>(string key)
        {
            var value = await Do(db => db.ListLeftPopAsync(key));
            return ConvertObj<T>(value);
        }

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        public async Task<long> ListLengthAsync(string key) =>
            await Do(redis => redis.ListLengthAsync(key));

        #endregion 异步方法

        #endregion List

        #region SortedSet 有序集合

        #region 同步方法

        /// <summary>
        /// 添加
        /// </summary>
        public bool SortedSetAdd<T>(string key, T value, double score) =>
            Do(redis => redis.SortedSetAdd(key, ConvertJson<T>(value), score));

        /// <summary>
        /// 删除
        /// </summary>
        public bool SortedSetRemove<T>(string key, T value) =>
            Do(redis => redis.SortedSetRemove(key, ConvertJson(value)));

        /// <summary>
        /// 获取全部
        /// </summary>
        public List<T> SortedSetRangeByRank<T>(string key)
        {
            return Do(redis =>
            {
                var values = redis.SortedSetRangeByRank(key);
                return ConvertList<T>(values);
            });
        }
        /// <summary>
        /// 获取全部
        /// </summary>
        public List<string> SortedSetRangeByRank(string key)
        {
            return Do(redis =>
            {
                var values = redis.SortedSetRangeByRank(key);
                return ConvertList(values);
            });
        }

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        public long SortedSetLength(string key) =>
            Do(redis => redis.SortedSetLength(key));

        #endregion 同步方法

        #region 异步方法

        /// <summary>
        /// 添加
        /// </summary>
        public async Task<bool> SortedSetAddAsync<T>(string key, T value, double score) =>
            await Do(redis => redis.SortedSetAddAsync(key, ConvertJson<T>(value), score));

        /// <summary>
        /// 删除
        /// </summary>
        public async Task<bool> SortedSetRemoveAsync<T>(string key, T value) =>
            await Do(redis => redis.SortedSetRemoveAsync(key, ConvertJson(value)));

        /// <summary>
        /// 获取全部
        /// </summary>
        public async Task<List<T>> SortedSetRangeByRankAsync<T>(string key)
        {
            var values = await Do(redis => redis.SortedSetRangeByRankAsync(key));
            return ConvertList<T>(values);
        }

        /// <summary>
        /// 获取集合中的数量
        /// </summary>
        public async Task<long> SortedSetLengthAsync(string key) =>
            await Do(redis => redis.SortedSetLengthAsync(key));

        #endregion 异步方法

        #endregion SortedSet 有序集合

        #region key

        /// <summary>
        /// 删除单个key
        /// </summary>
        /// <param name="key">redis key</param>
        /// <returns>是否删除成功</returns>
        public bool KeyDelete(string key) =>
            Do(db => db.KeyDelete(key));

        /// <summary>
        /// 删除多个key
        /// </summary>
        /// <param name="keys">rediskey</param>
        /// <returns>成功删除的个数</returns>
        public long KeyDelete(List<string> keys) =>
            Do(db => db.KeyDelete(ConvertRedisKeys(keys)));

        /// <summary>
        /// 判断key是否存储
        /// </summary>
        public bool KeyExists(string key) =>
            Do(db => db.KeyExists(key));

        /// <summary>
        /// 重新命名key
        /// </summary>
        public bool KeyRename(string key, string newKey) =>
            Do(db => db.KeyRename(key, newKey));

        /// <summary>
        /// 设置Key的时间
        /// </summary>
        public bool KeyExpire(string key, TimeSpan? expiry = default(TimeSpan?)) =>
            Do(db => db.KeyExpire(key, expiry));

        #endregion key

        #region 发布订阅

        /// <summary>
        /// Redis发布订阅  订阅
        /// </summary>
        public void Subscribe(string subChannel, Action<RedisChannel, RedisValue> handler = null)
        {
            ISubscriber sub = _conn.GetSubscriber();
            sub.Subscribe(subChannel, (channel, message) =>
            {
                if (handler == null)
                {
                    Console.WriteLine(subChannel + " 订阅收到消息：" + message);
                }
                else
                {
                    handler(channel, message);
                }
            });
        }

        /// <summary>
        /// Redis发布订阅  发布
        /// </summary>
        public long Publish<T>(string channel, T msg)
        {
            ISubscriber sub = _conn.GetSubscriber();
            return sub.Publish(channel, ConvertJson(msg));
        }

        /// <summary>
        /// Redis发布订阅  取消订阅
        /// </summary>
        /// <param name="channel"></param>
        public void Unsubscribe(string channel)
        {
            ISubscriber sub = _conn.GetSubscriber();
            sub.Unsubscribe(channel);
        }

        /// <summary>
        /// Redis发布订阅  取消全部订阅
        /// </summary>
        public void UnsubscribeAll()
        {
            ISubscriber sub = _conn.GetSubscriber();
            sub.UnsubscribeAll();
        }

        #endregion 发布订阅

        #region 其他

        public ITransaction CreateTransaction() => GetDatabase().CreateTransaction();

        public IDatabase GetDatabase() => _conn.GetDatabase(DbNum);

        public IServer GetServer(string hostAndPort) => _conn.GetServer(hostAndPort);

        /// <summary>
        /// 删除集合
        /// </summary>
        public bool KeyRemove(string redisKey)
        {
            var database = _conn.GetDatabase(DbNum);
            return database.KeyDelete(redisKey);
        }

        #endregion 其他

        #region 辅助方法

        private T Do<T>(Func<IDatabase, T> func)
        {
            var database = _conn.GetDatabase(DbNum);
            return func(database);
        }

        private string ConvertJson<T>(T value) =>
            value is string ? value.ToString() : JsonConvert.SerializeObject(value);

        private T ConvertObj<T>(RedisValue value)
        {
            if (value.IsNull)
                return default(T);
            T? t = default;
            return JsonConvert.DeserializeObject<T>(value) ?? t;
        }

        private List<T> ConvertList<T>(RedisValue[] values)
        {
            if (values == null)
                return null;

            List<T> result = new List<T>();
            foreach (var item in values)
            {
                var model = ConvertObj<T>(item);
                result.Add(model);
            }
            return result;
        }

        public List<string> ConvertList(RedisValue[] values)
        {
            if (values == null)
                return null;
            List<string> list = new List<string>();
            foreach (var value in values)
                list.Add(value);
            return list;
        }

        private List<string> ConvertString<T>(RedisValue[] values)
        {
            List<string> list = new List<string>();
            if (values == null)
                return null;
            foreach (RedisValue value2 in values)
                list.Add(value2.ToString());
            return list;
        }

        private RedisKey[] ConvertRedisKeys(List<string> redisKeys)
        {
            return redisKeys.Select(redisKey => (RedisKey)redisKey).ToArray();
        }

        #endregion 辅助方法
    }
}
