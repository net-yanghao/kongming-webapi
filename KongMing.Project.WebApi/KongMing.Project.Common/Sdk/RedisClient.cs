﻿using KongMing.Project.Entity.Common;
using StackExchange.Redis;
using System.Collections.Concurrent;

namespace KongMing.Project.Common.Sdk
{
    public class RedisClient: IDisposable
    {
        RedisConfig _redisConfig;
        private ConcurrentDictionary<string, ConnectionMultiplexer> _connections;
        public RedisClient(RedisConfig redisConfig)
        {
            _redisConfig = redisConfig;
            _connections = new ConcurrentDictionary<string, ConnectionMultiplexer>();
        }

        /// <summary>
        /// 获取ConnectionMultiplexer
        /// </summary>
        /// <returns></returns>
        private ConnectionMultiplexer GetConnect()
        {
            return _connections.GetOrAdd(_redisConfig.InstanceName, p => ConnectionMultiplexer.Connect(_redisConfig.ConnectionString));
        }

        /// <summary>
        /// 获取数据库
        /// </summary>
        /// <param name="configName"></param>
        /// <param name="db">默认为0：优先代码的db配置，其次config中的配置</param>
        /// <returns></returns>
        public IDatabase GetDatabase()
        {
            return GetConnect().GetDatabase(_redisConfig.DefaultDB);
        }

        public IDatabase GetDatabase(int dbId)
        {
            return GetConnect().GetDatabase(dbId);
        }

        public IServer GetServer(string configName = null, int endPointsIndex = 0)
        {
            var confOption = ConfigurationOptions.Parse(_redisConfig.ConnectionString);
            return GetConnect().GetServer(confOption.EndPoints[endPointsIndex]);
        }

        public ISubscriber GetSubscriber(string configName = null)
        {
            return GetConnect().GetSubscriber();
        }

        public void Dispose()
        {
            if (_connections != null && _connections.Count > 0)
            {
                foreach (var item in _connections.Values)
                {
                    item.Close();
                }
            }
        }
    }
}
