﻿using KongMing.Project.Entity.Business;
using KongMing.Project.Entity.Common;
using KongMing.Project.Entity.Enum;
using System.Linq.Expressions;
using static KongMing.Project.Common.ExpressionHelper;

namespace KongMing.Project.Common
{
    /// <summary>
    /// Lambda查询帮助类
    /// </summary>
    public class LambdaQueryHelper
    {
        /// <summary>
        /// 将查询参数转为表达式
        /// </summary>
        public static ApiQueryExpression<T> GetQueryExpsByApiQuery<T>(BaseApiQuery apiQuery) where T : BaseDBEntity
        {
            ApiQueryExpression<T> apiQueryExpression = new ApiQueryExpression<T>();
            //条件lambda
            apiQueryExpression.whereExp = apiQuery?.ConditionItems?.Count > 0 ? GetWhereExp<T>(apiQuery.ConditionItems) : p => true;
            //排序语句
            apiQueryExpression.orderStr = apiQuery?.OrderItems?.Count > 0 ? GetOrderStr(apiQuery.OrderItems) : " CreateTime desc ";
            return apiQueryExpression;
        }

        /// <summary>
        /// 获取查询条件
        /// </summary>
        public static Expression<Func<T, bool>> GetWhereExp<T>(List<ConditionItem> conditionItems)
        {
            Expression<Func<T, bool>> whereExp = p => true;
            foreach (var item in conditionItems)
            {

                Expression<Func<T, bool>> exp = item.Operator switch
                {
                    QueryConditionOperator.模糊匹配 => CreateContains<T>(item.Field, item.Value.ToString()),
                    QueryConditionOperator.等于 => CreateEqual<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.不等于 => CreateNotEqual<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.大于 => CreateGreaterThan<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.大于等于 => CreateGreaterThanOrEqual<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.小于 => CreateLessThan<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.小于等于 => CreateLessThanOrEqual<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.在_之中 => GetWhere_InorNotIn_Exp<T>(item.Field, item.Value, item.ValueType),
                    QueryConditionOperator.不在_之中 => GetWhere_InorNotIn_Exp<T>(item.Field, item.Value, item.ValueType, false)
                };
                whereExp = whereExp.And(exp);
            }
            return whereExp;
        }

        /// <summary>
        /// 获取排序语句
        /// </summary>
        public static string GetOrderStr(List<OrderItem> orderItems)
        {
            string orderStr = string.Empty;
            foreach (OrderItem item in orderItems)
            {
                orderStr += $"{item.Field} {Enum.GetName(typeof(QueryOrderType), item.OrderType)},";
            }
            if (orderItems.Count > 0)
                orderStr = orderStr.Substring(0, orderStr.Length - 1);
            return orderStr;
        }
    }
}
