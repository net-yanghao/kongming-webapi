﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KongMing.Project.Common
{
    public static class Checker
    {
        public static bool NotNull(this string text) => !string.IsNullOrWhiteSpace(text);
        public static void CheckNull(this string text, string paramName = "", string msg = "")
        {
            if (!string.IsNullOrWhiteSpace(text))
                return;
            if (!string.IsNullOrWhiteSpace(msg))
                throw new Exception(msg);
            if (!string.IsNullOrWhiteSpace(paramName))
                throw new ArgumentException($"The parameter '{paramName}' value cannot be null.");
            throw new ArgumentNullException();
        }

        public static void CheckNull(this object obj, string paramName = "", string msg = "")
        {
            if (obj != null)
                return;
            if (!string.IsNullOrWhiteSpace(msg))
                throw new Exception(msg);
            if (!string.IsNullOrWhiteSpace(paramName))
                throw new ArgumentException($"The parameter '{paramName}' value cannot be null.");
            throw new ArgumentNullException();
        }

        public static void CheckEnum<TEnum>(this TEnum _enum, string msg = "") where TEnum : struct, Enum
        {
            if (Enum.IsDefined(_enum))
                return;
            if (!string.IsNullOrWhiteSpace(msg))
                throw new Exception(msg);
            throw new ArgumentException($"The parameter '{typeof(TEnum).Name}' enum value error.");
        }

        public static void CheckZero(this int number, string paramName = "", string msg = "")
        {
            if (number != 0)
                return;
            if (!string.IsNullOrWhiteSpace(msg))
                throw new Exception(msg);
            if (!string.IsNullOrWhiteSpace(paramName))
                throw new ArgumentException($"The parameter '{paramName}' value cannot be 0 or null.");
            throw new ArgumentNullException();
        }

        public static void Check(this bool isTrue, string msg)
        {
            if (!isTrue)
                throw new Exception(msg);
        }

        /// <summary>
        /// 检查数据库用户密码
        /// 以英文开头，包含英文和数字，长度：10 - 16
        /// </summary>
        public static void CheckDbPassword(this string password, string msg)
        {
            if(string.IsNullOrWhiteSpace(password))
                throw new Exception(msg);
            //以英文开头，必须同时包含英文和数字，长度：10 - 16
            if (!Regex.IsMatch(password, @"^[A-Za-z][A-Za-z0-9]{9,15}$"))
                throw new Exception(msg);
            if (Regex.IsMatch(password, @"^[A-Za-z]*$"))
                throw new Exception(msg);
        }

        public static bool IsInteger(this string txt)
        {
            if (string.IsNullOrWhiteSpace(txt))
                return false;
            if (!Regex.IsMatch(txt, @"^[0-9]*$"))
                return false;
            return true;
        }

        public static bool IsMobile(this string mobile)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return false;
            if (!Regex.IsMatch(mobile, @"^[1]+[3,4,5,6,7,8,9]+\d{9}")) return false;
            return true;
        }

        public static bool IsEmail(this string email)
        {
            email = email ?? "";
            if (Regex.IsMatch(email, "\\s")) return false;
            var index1 = email.IndexOf('@');
            if (index1 <= 0) return false;
            var index2 = email.IndexOf('.', index1);
            if (index2 <= 0 || index2 < index1 + 2) return false;
            return true;
        }
    }
}
