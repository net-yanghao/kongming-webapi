﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace KongMing.Project.Common
{
    /// <summary>
    /// 正则表达式类
    /// </summary>
    public static class RegularHelper
    {
        /// <summary>
        /// 匹配int：6
        /// </summary>
        public static bool ReIsInt(this string str) => 
            Regex.IsMatch(str, @"^[+-]?\d*$");

        /// <summary>
        /// 匹配double：6.6
        /// </summary>
        public static bool ReIsDouble(this string str) =>
            Regex.IsMatch(str, @"^([0-9]{1,}[.][0-9]*)$");

        /// <summary>
        /// 匹配date：yyyy-MM-dd
        /// </summary>
        public static bool ReIsDate(this string str) =>
            Regex.IsMatch(str, @"^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-9]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$");

        /// <summary>
        /// 匹配time：HH:mm:ss
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool ReIsTime(this string str)=>
            Regex.IsMatch(str, @"^((20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d)$");

        /// <summary>
        /// 匹配DateTime：yyyy-MM-dd HH:mm:ss
        /// </summary>
        public static bool ReIsDateTime(this string str)=>
            Regex.IsMatch(str, @"^(((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d)$ ");

        /// <summary>
        /// 匹配手机号码
        /// </summary>
        public static bool ReIsTel(this string str) =>
            Regex.IsMatch(str, @"\d{3}-\d{8}|\d{4}-\d{7}");
    }
}
