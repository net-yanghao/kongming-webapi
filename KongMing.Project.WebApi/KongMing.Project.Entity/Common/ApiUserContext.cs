﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Common
{
    /// <summary>
    /// 每次Api请求中的用户身份信息
    /// </summary>
    public class ApiUserContext
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserAccount { get; set; }
        
        /// <summary>
        /// Jwt身份令牌
        /// </summary>
        public string JwtToken { get; set; }

        /// <summary>
        /// url全部信息
        /// </summary>
        public Uri FullUrl { get; set; }

        /// <summary>
        /// url的origin部分：http://localohst:5001
        /// </summary>
        public string OriginUrl { get; set; }
    }
}
