﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.Entity.Common
{
    /// <summary>
    /// 接口返回类
    /// </summary>
    public class BaseApiResult
    {
        public int code { get; set; }

        public long timestamp
        {
            get
            {
                return GetTimeStamp();
            }
        }

        public string msg { get; set; }

        public object data { get; set; }

        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        /// <summary>
        /// 保存数据常用
        /// </summary>
        /// <param name="res">操作数据库返回条数</param>
        public static BaseApiResult SaveResult(int res)
        {
            if (res == 0)
                return Error("保存失败！");
            else
                return Success("保存成功！");
        }

        /// <summary>
        /// 删除数据常用
        /// </summary>
        /// <param name="res">操作数据库返回条数</param>
        public static BaseApiResult DeleteResult(int res)
        {
            if (res == 0)
                return Error("删除失败！");
            else
                return Success("删除成功！");
        }

        /// <summary>
        /// 请求成功
        /// </summary>
        public static BaseApiResult Success(string msg, object data = null)
        {
            return new BaseApiResult { code = 0, msg = string.IsNullOrEmpty(msg) ? "请求成功！" : msg, data = data };
        }

        /// <summary>
        /// 请求成功
        /// </summary>
        public static BaseApiResult Success(object data = null)
        {
            return new BaseApiResult { code = 0, msg = "请求成功！", data = data };
        }

        /// <summary>
        /// 请求失败
        /// </summary>
        public static BaseApiResult Error(string msg = "", object data = null)
        {
            return new BaseApiResult { code = 10, msg = msg, data = data };
        }
    }
}
