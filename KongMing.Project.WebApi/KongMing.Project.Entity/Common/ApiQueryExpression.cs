﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KongMing.Project.Entity.Business;
using SqlSugar;

namespace KongMing.Project.Entity.Common
{
    public class ApiQueryExpression<T> where T : BaseDBEntity
    {
        /// <summary>
        /// 条件语句
        /// </summary>
        public Expression<Func<T, bool>> whereExp { get; set; }
        /// <summary>
        /// 查询语句
        /// </summary>
        public Expression<Func<T, bool>> selectExp { get; set; }

        /// <summary>
        /// 排序类
        /// </summary>
        public string orderStr { get; set; }

    }
}
