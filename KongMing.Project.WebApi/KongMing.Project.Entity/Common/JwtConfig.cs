﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Common
{
    public class JwtConfig
    {
        public string? Audience { get; set; }
        public string? Issuer { get; set; }
        public string? SecurityKey { get; set; }
        /// <summary>
        /// token有效时间，分钟
        /// </summary>
        public double EffectiveMinutes { get; set; } = 5;
        /// <summary>
        /// jwt秘钥的加密方式：0对称可逆加密，1非对称可逆加密 
        /// </summary>
        public int KeySymmetryType { get; set; } = 0;
    }
}
