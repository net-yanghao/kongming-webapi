﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Common
{
    public class QueryModel<T>
    {
        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 20;

        public List<OrderItem>? OrderItems { get; set; }

        public T? Model { get; set; }

        public string GetOrderStr()
        {
            if (OrderItems == null || OrderItems.Count == 0)
                return "";
            string orderStr = "";
            foreach (var item in OrderItems)
            {
                var order = item.OrderType == 0 ? "asc" : "desc";
                orderStr += $" {item.Field} {order}, ";
            }
            return orderStr.TrimEnd(',');
        }
    }
}
