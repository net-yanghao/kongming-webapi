﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KongMing.Project.Entity.Enum;

namespace KongMing.Project.Entity.Common
{
    /// <summary>
    /// 查询参数类
    /// </summary>
    public class BaseApiQuery
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页数据条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 条件参数集合
        /// </summary>
        public List<ConditionItem> ConditionItems { get; set; }

        /// <summary>
        /// 排序参数集合
        /// </summary>
        public List<OrderItem> OrderItems { get; set; }
    }

    /// <summary>
    /// 查询条件参数类
    /// </summary>
    public class ConditionItem
    {
        /// <summary>
        /// 条件字段名
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// 查询条件计算符
        /// </summary>
        public QueryConditionOperator Operator { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// 值数据类型，string,int,double,datetime,bool
        /// </summary>
        public string ValueType { get; set; }
        //与下一个条件的连接符
        //public string connector { get; set; }
    }

    /// <summary>
    /// 查询排序参数类
    /// </summary>
    public class OrderItem
    {
        public string Field { get; set; }
        public QueryOrderType OrderType { get; set;  } = 0;
    }
}
