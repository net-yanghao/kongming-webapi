﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Common
{
    public class RedisConfig
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 实例名
        /// </summary>
        public string InstanceName { get; set; }
        /// <summary>
        /// 默认数据库
        /// </summary>
        public int DefaultDB { get; set; } = 0;
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}
