﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Common
{
    public class QueryResult<T>
    {
        /// <summary>
        /// 当前页码
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 每页数据条数
        /// </summary>
        public int PageSize { get; set; } = 20;
        /// <summary>
        /// 数据总数
        /// </summary>
        public int DataCount { get; set; } = 0;
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount { get; set; } = 0;
        /// <summary>
        /// 数据集合
        /// </summary>
        public List<T> Data { get; set; } = new List<T>();

        //计算总页数
        public int ChangePageCount()
        {
            if (DataCount > 0)
                PageCount = (int)Math.Ceiling((decimal)DataCount / PageSize);
            return PageCount;
        }
    }
}
