﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.View
{
    public class User_V
    {
        public string Id { get; set; }
        public string Account { get; set; }
        public string Pwd { get; set; }
        public string Name { get; set; }
        public int Age { get; set; } = 0;
        public int Sex { get; set; } = 0;
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
