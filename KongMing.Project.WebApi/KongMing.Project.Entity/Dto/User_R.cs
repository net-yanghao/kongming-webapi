﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Dto
{
    public class User_R
    {
        public string? Account { get; set; }
        public string? Name { get; set; }
        public int? Age { get; set; }
        public int? Sex { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
    }
}
