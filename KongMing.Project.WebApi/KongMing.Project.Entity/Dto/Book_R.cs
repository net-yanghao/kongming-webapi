﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Dto
{
    public class Book_R
    {
        public string? BookId { get; set; }

        public string? Title { get; set; }

        public string? Author { get; set; }

        public string? Description { get; set; }

        public decimal? InPrice { get; set; }

        public decimal? SellPrice { get; set; }
    }
}
