﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Enum
{
    public enum ErrorCode
    {
        BusinessError = 100,//业务错误
        TokenError = 401,//token令牌身份验证不通过
        CodeError = 500,//代码错误抛异常
    }
}
