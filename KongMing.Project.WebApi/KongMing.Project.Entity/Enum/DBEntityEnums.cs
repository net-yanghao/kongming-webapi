﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Enum
{
    public enum SysConfigValueType
    {
        String = 0,//字符串
        Int = 1,//整数
        Decimal = 2,//小数
        Boolean = 3,//布尔值
        Json = 4,//对象
        Array = 5,//数组
    }
}
