﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Enum
{
    /// <summary>
    /// 查询条件计算符
    /// </summary>
    public enum QueryConditionOperator
    {
        模糊匹配 = 0,
        等于 = 1,
        不等于 = -1,        
        大于 = 10,
        大于等于 = 11,
        小于 = 20,
        小于等于 = 21,
        在_之中 = 40, //多个值用,隔开
        不在_之中 = -40 //多个值用,隔开
    }
}
