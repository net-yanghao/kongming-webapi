﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.Entity.Business
{
    /// <summary>
    /// 登录信息记录表
    /// </summary>
    public class SysAuthToken
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true, ColumnDataType = "int")]
        public int Id { get; set; }

        /// <summary>
        /// JWT生成的身份认证令牌
        /// </summary>
        [SugarColumn(Length = 2000, DefaultValue = "''", ColumnDataType = "varchar")]
        public string AuthToken { get; set; } = "";

        /// <summary>
        /// Jwt的加密类型：0对称可逆加密，1非对称可逆加密
        /// </summary>
        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int KeySymmetryType { get; set; } = 0;

        /// <summary>
        /// 登录用户的Id
        /// </summary>
        [SugarColumn(Length = 32, DefaultValue = "''", ColumnDataType = "varchar")]
        public string UserId { get; set; } = "";

        /// <summary>
        /// 登录用户的姓名
        /// </summary>
        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string UserName { get; set; } = "";

        /// <summary>
        /// 登录时间
        /// </summary>
        [SugarColumn(Length = 20, DefaultValue = "''", ColumnDataType = "varchar")]
        public string LoginTime { get; set; } = "";

        /// <summary>
        /// token状态：0正常，-1失效，系统强制控制身份认证失效
        /// </summary>
        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int Status { get; set; } = 0;

        /// <summary>
        /// 登录用户（客户端）的IP
        /// </summary>
        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Ip { get; set; } = "";

    }
}
