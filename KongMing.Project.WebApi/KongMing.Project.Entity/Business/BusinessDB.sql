--图书信息表
create table [Book]
(
[BookId] varchar(32) not null,--图书id 
[Sn] int not null identity(1,1),--序号 
[Title] varchar(100) not null default (''),--书名 
[Author] varchar(50) not null default (''),--作者 
[Description] varchar(500) not null default (''),--备注 
[InPrice] decimal(18,2) not null default ('0'),--进价 
[SellPrice] decimal(18,2) not null default ('0'),--售价 
[CreateTime] varchar(20) not null default (''), 
[CreateUserId] varchar(32) not null default (''), 
[CreateUserName] varchar(50) not null default (''), 
[ModifyTime] varchar(20) not null default (''), 
[ModifyUserId] varchar(32) not null default (''), 
[ModifyUserName] varchar(50) not null default (''), 
constraint PK_Book primary key(BookId)
)

--系统菜单表
create table [SysMenu]
(
[MenuId] varchar(32) not null,--菜单id 
[MenuName] varchar(100) not null default (''),--菜单名 
[PId] varchar(32) not null default (''),--父级菜单id 
[Icon] varchar(100) not null default (''),--图标 
[Sort] int not null default ('0'),--排序 
[Href] varchar(300) not null default (''),--跳转连接 
[Remark] varchar(300) not null default (''),--备注 
[CreateTime] varchar(20) not null default (''), 
[CreateUserId] varchar(32) not null default (''), 
[CreateUserName] varchar(50) not null default (''), 
[ModifyTime] varchar(20) not null default (''), 
[ModifyUserId] varchar(32) not null default (''), 
[ModifyUserName] varchar(50) not null default (''), 
constraint PK_SysMenu primary key(MenuId)
)

--登录信息记录表
create table [SysAuthToken]
(
[Id] int not null identity(1,1), 
[AuthToken] varchar(2000) not null default (''),--JWT生成的身份认证令牌 
[KeySymmetryType] int not null default ('0'),--Jwt的加密类型：0对称可逆加密，1非对称可逆加密 
[UserId] varchar(32) not null default (''),--登录用户的Id 
[UserName] varchar(50) not null default (''),--登录用户的姓名 
[LoginTime] varchar(20) not null default (''),--登录时间 
[Status] int not null default ('0'),--token状态：0正常，-1失效，系统强制控制身份认证失效 
[Ip] varchar(50) not null default (''),--登录用户（客户端）的IP 
constraint PK_SysAuthToken primary key(Id)
)

--用户信息表
create table [SysUser]
(
[UserId] varchar(32) not null, 
[Account] varchar(50) not null default (''), 
[Password] varchar(50) not null default (''), 
[Name] varchar(50) not null default (''), 
[Age] int not null default ('0'), 
[Sex] int not null default ('0'),--性别，0女1男 
[Phone] varchar(20) not null default (''), 
[Address] varchar(200) not null default ('') , 
[IsDelete] int not null default ('0'),--是否删除，0否1是 
[CreateTime] varchar(20) not null default (''), 
[CreateUserId] varchar(32) not null default (''), 
[CreateUserName] varchar(50) not null default (''), 
[ModifyTime] varchar(20) not null default (''), 
[ModifyUserId] varchar(32) not null default (''), 
[ModifyUserName] varchar(50) not null default (''),
constraint PK_SysUser primary key(UserId)
)