﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.Entity.Business
{
    /// <summary>
    /// 用户信息表
    /// <summary>
    public class SysUser : BaseDBEntity
    {
        [SugarColumn(Length = 32, IsPrimaryKey = true, ColumnDataType = "varchar")]
        public string UserId { get; set; }

        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Account { get; set; } = "";

        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Password { get; set; } = "";

        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Name { get; set; } = "";

        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int Age { get; set; } = 0;

        /// <summary>
        /// 性别，0女1男
        /// <summary>
        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int Sex { get; set; } = 0;

        [SugarColumn(Length = 20, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Phone { get; set; } = "";

        [SugarColumn(Length = 200, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Address { get; set; } = "";

        /// <summary>
        /// 是否删除，0否1是
        /// <summary>
        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int IsDelete { get; set; } = 0;

    }

}
