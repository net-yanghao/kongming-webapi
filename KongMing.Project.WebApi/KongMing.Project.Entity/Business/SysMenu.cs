﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Business
{
    /// <summary>
    /// 系统菜单表
    /// </summary>
    [SugarTable("SysMenu")]
    public class SysMenu : BaseDBEntity
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        [SugarColumn(Length = 32, IsPrimaryKey = true, ColumnDataType = "varchar")]
        public string MenuId { get; set; }

        /// <summary>
        /// 菜单名
        /// </summary>
        [SugarColumn(Length = 100, DefaultValue = "''", ColumnDataType = "varchar")]
        public string MenuName { get; set; } = "";

        /// <summary>
        /// 父级菜单id
        /// </summary>
        [SugarColumn(Length = 32, DefaultValue = "''", ColumnDataType = "varchar")]
        public string PId { get; set; } = "";

        /// <summary>
        /// 图标
        /// </summary>
        [SugarColumn(Length = 100, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Icon { get; set; } = "";

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(DefaultValue = "0", ColumnDataType = "int")]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 跳转连接
        /// </summary>
        [SugarColumn(Length = 300, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Href { get; set; } = "";

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 300, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Remark { get; set; } = "";

    }
}
