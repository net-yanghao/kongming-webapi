﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KongMing.Project.Entity.Business
{
    /// <summary>
    /// 图书信息表
    /// </summary>
    [SugarTable("Book")]
    public class Book : BaseDBEntity
    {
        /// <summary>
        /// 图书id
        /// </summary>
        [SugarColumn(Length = 32, IsPrimaryKey = true, ColumnDataType = "varchar")]
        public string BookId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [SugarColumn(IsIdentity = true, ColumnDataType = "int")]
        public int Sn { get; set; }

        /// <summary>
        /// 书名
        /// </summary>
        [SugarColumn(Length = 100, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Title { get; set; } = "";

        /// <summary>
        /// 作者
        /// </summary>
        [SugarColumn(Length = 50, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Author { get; set; } = "";

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 500, DefaultValue = "''", ColumnDataType = "varchar")]
        public string Description { get; set; } = "";

        /// <summary>
        /// 进价
        /// </summary>
        [SugarColumn(Length = 18, DecimalDigits = 2, DefaultValue = "0", ColumnDataType = "decimal")]
        public decimal InPrice { get; set; } = 0;

        /// <summary>
        /// 售价
        /// </summary>
        [SugarColumn(Length = 18, DecimalDigits = 2, DefaultValue = "0", ColumnDataType = "decimal")]
        public decimal SellPrice { get; set; } = 0;

    }
}
