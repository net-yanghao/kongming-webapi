﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.Entity.Business
{
    public class BaseDBEntity
    {
        [SugarColumn(Length = 20, DefaultValue = "''")]
        public string CreateTime { get; set; } = "";

        [SugarColumn(Length = 32, DefaultValue = "''")]
        public string CreateUserId { get; set; } = "";

        [SugarColumn(Length = 50, DefaultValue = "''")]
        public string CreateUserName { get; set; } = "";

        [SugarColumn(Length = 20, DefaultValue = "''")]
        public string ModifyTime { get; set; } = "''";

        [SugarColumn(Length = 32, DefaultValue = "''")]
        public string ModifyUserId { get; set; } = "";

        [SugarColumn(Length = 50, DefaultValue = "''")]
        public string ModifyUserName { get; set; } = "";
    }
}
