﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.Entity.Business
{
    /// <summary>
    /// 项目配置
    /// </summary>
    public class SysConfig
    {
        /// <summary>
        /// 配置的主键
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, Length = 50, ColumnDataType = "varchar(50)")]
        public string Key { get; set; }
        /// <summary>
        /// 配置的名称
        /// </summary>
        [SugarColumn(Length = 100, ColumnDataType = "varchar(100)", DefaultValue = "")]
        public string Name { get; set; }
        /// <summary>
        /// 配置的值
        /// </summary>
        [SugarColumn(ColumnDataType = "text", DefaultValue = "")]
        public object Value { get; set; }
        /// <summary>
        /// 配置的值类型
        /// </summary>
        [SugarColumn(Length = 100, ColumnDataType = "varchar(100)", DefaultValue = "")]
        public string ValueType { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        [SugarColumn(ColumnDataType = "text", DefaultValue = "")]
        public object DefaultValue { get; set; } = "";
        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 1000, ColumnDataType = "varchar(1000)", DefaultValue = "")]
        public string Remark { set; get; } = "";
    }
}
