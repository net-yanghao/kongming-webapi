﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace KongMing.Project.DBRepository
{
    public class BaseDBEntity
    {
        [SugarColumn(IsPrimaryKey =true)]
        public string Id { get; set; }

        [SugarColumn(DefaultValue = "")]
        public string CreateTime { get; set; }

        [SugarColumn(DefaultValue = "")]
        public string CreateUserId { get; set; }

        [SugarColumn(DefaultValue = "")]
        public string CreateUserName { get; set; }

        [SugarColumn(DefaultValue = "")]
        public string ModifyTime { get; set; } = "";

        [SugarColumn(DefaultValue = "")]
        public string ModifyUserId { get; set; } = "";

        [SugarColumn(DefaultValue = "")]
        public string ModifyUserName { get; set; } = "";

        [SugarColumn(DefaultValue = "0")]
        public int IsDelete { get; set; } = 0;
    }
}
