# KongMing.Project.WebApi

#### 介绍
使用.NET6 WebApi SqlSugar CodeFirst框架。包含功能：
1、JWT身份验证，对称可逆、非对称可逆加密封装。
2、每次请求时获取JWT Token 中的登录用户的信息。
3、Redis、RabbitMQ 中间件的使用拓展。
4、全局异常处理过滤器、全局api接口返回数据格式处理过滤器。
5、通用查询帮助类，将查询信息转查询lambda表达式，万能查询方法。

#### 项目框架
![输入图片说明](https://foruda.gitee.com/images/1662111892054800615/d2275759_10137268.png "项目结构.png")

#### 项目代码案例
![输入图片说明](https://foruda.gitee.com/images/1662111940093880038/b207adf5_10137268.png "swagger.png")
![输入图片说明](https://foruda.gitee.com/images/1662111951214054491/8bb45351_10137268.png "代码案例.png")